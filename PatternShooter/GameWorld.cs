﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace PatternShooter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameWorld : Game
    {
        static GameWorld instance;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<GameObject> gameObjects;

        static List<GameObject> gameObjectsToBeAdded;
        static List<GameObject> gameObjectsToBeRemoved;


        public static GameWorld Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameWorld();
                }
                return instance;
            }
        }

        private GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 800;
            Content.RootDirectory = "Content";

            gameObjects = new List<GameObject>();
            gameObjectsToBeAdded = new List<GameObject>();
            gameObjectsToBeRemoved = new List<GameObject>();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            GameObject spiller = PlayerFactory.Instance.Create("normal", new Vector2((graphics.PreferredBackBufferWidth - 112) / 2, graphics.PreferredBackBufferHeight - 150));
            
            gameObjects.Add(spiller);
            gameObjects.Add(EnemyFactory.Instance.Create("simpleGreen", new Vector2(100, 0)));
            gameObjects.Add(EnemyFactory.Instance.Create("simpleRed", new Vector2(300, 0)));
            gameObjects.Add(LaserFactory.Instance.Create(spiller.Transform.Position));

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            foreach (GameObject obj in gameObjects)
            {
                obj.LoadContent(Content);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            foreach (GameObject obj in gameObjects)
            {
                obj.Update(gameTime);
            }

            // Remove objects that are queued to be removed
            foreach (GameObject obj in gameObjectsToBeRemoved)
            {
                gameObjects.Remove(obj);
            }
            gameObjectsToBeRemoved.Clear();

            // Add objects that are queued to be added
            foreach (GameObject obj in gameObjectsToBeAdded)
            {
                gameObjects.Add(obj);
            }
            gameObjectsToBeRemoved.Clear();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            foreach (GameObject obj in gameObjects)
            {
                obj.Draw(spriteBatch);
            }

            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        public static void AddGameObject(GameObject obj)
        {
            gameObjectsToBeAdded.Add(obj);
        }

        public static void RemoveGameObject(GameObject obj)
        {
            gameObjectsToBeRemoved.Add(obj);
        }
    }
}
