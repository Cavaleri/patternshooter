﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class MoveCommand : ICommand
    {
        Vector2 velocity;

        public MoveCommand(Vector2 vel)
        {
            this.velocity = vel;
        }

        public void Exectute(Player player)
        {
            player.Move(this.velocity);
        }
    }
}
