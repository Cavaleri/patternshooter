﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class GameObject
    {
        List<Component> components;
        Transform transform;

        public Transform Transform
        {
            get { return transform;  }
        }

        public GameObject(Vector2 startPos)
        {
            components = new List<Component>();
            this.transform = new Transform(this, startPos);
            AddComponent(transform);

        }

        public GameObject()
        {
            components = new List<Component>();
            this.transform = new Transform(this, Vector2.Zero);
            AddComponent(transform);
        }

        public void AddComponent(Component component)
        {
            component.Attach(this);
            components.Add(component);
        }

        public Component GetComponent(string componentName)
        {
            return components.FirstOrDefault((component) =>
            {
                return (component.ToString() == "PatternShooter."+componentName);
            });
        }

        public void LoadContent(ContentManager content)
        {
            foreach (Component component in components)
            {
                component.LoadContent(content);
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (Component component in components)
            {
                component.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Component component in components)
            {
                component.Draw(spriteBatch);
            }
        }
    }
}
