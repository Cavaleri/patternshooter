﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class Transform : Component
    {
        public Vector2 Position { get; set; }

        public Transform (GameObject gameObject, Vector2 position)
        {
            this.Position = position;
        }

        public void Translate(Vector2 translation)
        {
            Position += translation;
        }
    }
}
