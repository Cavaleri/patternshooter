﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public abstract class Enemy : Component
    {
        private float speed;
        private Vector2 velocity;
        private float deltaTime;

        protected IState currentState;

        public float Speed { get => speed; set => speed = value; }
        public Vector2 Velocity { get => velocity; set => velocity = value; }
        public float DeltaTime { get => deltaTime; set => deltaTime = value; }

        public Enemy()
        {
            this.Speed = 0f;
            this.Velocity = Vector2.Zero;
        }

        public void ChangeState(IState nextState)
        {
            if (this.currentState != null)
            {
                this.currentState.Exit();
            }
            this.currentState = nextState;
            this.currentState.Enter(this.GameObject);
        }
    }
}
