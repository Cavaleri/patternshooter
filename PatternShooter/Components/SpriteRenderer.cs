﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class SpriteRenderer : Component
    {
        Rectangle viewRect;
        Texture2D sprite;

        Rectangle[] viewRects;
        bool animationLoops;
        float animationFPS;
        int animationCurrentIndex;
        double timeElapsed;
        int animationFrames;

        int[] stillFramesMoving;

        string spritename;

        public SpriteRenderer(string spriteName, int frames, int fps)
        {
            this.spritename = spriteName;
            this.animationFrames = frames;
            this.animationFPS = fps;
            this.animationLoops = true; // All animations loop right now
        }

        public override void LoadContent(ContentManager content)
        {
            this.viewRects = new Rectangle[animationFrames];

            this.stillFramesMoving = new int[animationFrames];
            for (int i = 0; i < this.stillFramesMoving.Length; i++)
            {
                this.stillFramesMoving[i] = i;
            }

            this.sprite = content.Load<Texture2D>(this.spritename);

            for (int i = 0; i < animationFrames; i++)
            {
                viewRects[i] = new Rectangle(i * (sprite.Width / animationFrames), 0, (sprite.Width / animationFrames), sprite.Height);
            }
        }

        public override void Update(GameTime gameTime)
        {
            HandleAnimations(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.sprite, this.GameObject.Transform.Position, this.viewRects[animationCurrentIndex], Color.White);
        }

        private void HandleAnimations(GameTime gameTime)
        {

            this.timeElapsed += gameTime.ElapsedGameTime.TotalSeconds;
            int tempIndex = (int)(timeElapsed * animationFPS);

            // Reset the animation if looping
            if (tempIndex > stillFramesMoving.Length - 1)
            {
                if (this.animationLoops)
                {
                    tempIndex = 0;
                    timeElapsed = 0;
                }
            }

            this.animationCurrentIndex = stillFramesMoving[tempIndex];
        }

        public SpriteRenderer Clone()
        {
            return (SpriteRenderer) this.MemberwiseClone();
        }
    }
}
