﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class Laser : Component
    {
        float speed;
        Vector2 velocity;
        float deltaTime;

        public Laser(float speed, Vector2 velocity)
        {
            this.speed = speed;
            this.velocity = velocity;
        }

        public void Move()
        {
            if (velocity != Vector2.Zero)
            {
                velocity.Normalize();
            }
            velocity *= speed;
            base.GameObject.Transform.Translate(velocity * deltaTime);

            if (base.GameObject.Transform.Position.Y < 0)
            {
                GameWorld.RemoveGameObject(base.GameObject);
            }
        }

        public override void Update(GameTime gameTime)
        {
            this.deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Move();
        }
    }
}
