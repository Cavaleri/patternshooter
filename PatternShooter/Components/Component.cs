﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public abstract class Component
    {
        private GameObject gameObject;

        public GameObject GameObject
        {
            get { return gameObject; }
        }

        public virtual void Attach(GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public virtual void LoadContent(ContentManager content)
        {

        }

        public virtual void Update (GameTime gameTime)
        {

        }

        public virtual void Draw (SpriteBatch spriteBatch)
        {

        }
    }
}
