﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PatternShooter
{
    public class EasyEnemy : Enemy
    {
        float totalElapsed = 0;

        public EasyEnemy() : base()
        {

        }

        public override void Update(GameTime gameTime)
        {
            base.DeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            totalElapsed += base.DeltaTime;

            if (totalElapsed > 0.5f)
            {
                if (base.currentState.ToString() == "PatternShooter.MovingDownState")
                {
                    base.ChangeState(new DodgeLeftState());
                }
                else if (base.currentState.ToString() == "PatternShooter.DodgeLeftState")
                {
                    base.ChangeState(new MovingDownState());
                }

                totalElapsed = 0f;
            }

            base.currentState.Execute();
        }
    }
}
