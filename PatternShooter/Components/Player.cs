﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class Player : Component
    {
        float speed;
        float deltaTime;

        public Player ()
        {
            this.speed = 450f;
        }

        public override void Update (GameTime gameTime)
        {
            this.deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            InputHandler.Instance.Execute(this);
        }

        public void Move(Vector2 velocity)
        {
            if (velocity != Vector2.Zero)
            {
                velocity.Normalize();
            }
            velocity *= speed;
            this.GameObject.Transform.Translate(velocity * deltaTime);
        }        
    }
}
