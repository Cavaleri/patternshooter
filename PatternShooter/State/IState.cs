﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public interface IState
    {
        void Enter(GameObject entity);
        void Execute();
        void Exit();
    }
}
