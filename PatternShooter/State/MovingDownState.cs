﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class MovingDownState : IState
    {
        private GameObject parent;
        private Enemy parentEnemy;

        public void Enter(GameObject entity)
        {
            this.parent = entity;
            this.parentEnemy = (Enemy)entity.GetComponent("EasyEnemy");
            this.parentEnemy.Speed = 200f;
            this.parentEnemy.Velocity = new Vector2(0, 1);
        }

        public void Execute()
        {
            if (parentEnemy.Velocity != Vector2.Zero)
            {
                parentEnemy.Velocity.Normalize();
            }

            parent.Transform.Translate(parentEnemy.Velocity * parentEnemy.Speed * parentEnemy.DeltaTime);
        }

        public void Exit()
        {
            
        }
    }
}
