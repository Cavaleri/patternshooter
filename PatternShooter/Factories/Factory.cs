﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public abstract class Factory
    {
        public virtual GameObject Create(Vector2 startPos)
        {
            return null;
        }

        public virtual GameObject Create(string type, Vector2 startPos)
        {
            return null;
        }
    }
}
