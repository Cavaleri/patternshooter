﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PatternShooter
{
    class EnemyFactory : Factory
    {
        Dictionary<string, SpriteRenderer> spriteRendererPrototypes;
        static EnemyFactory instance;

        public static EnemyFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EnemyFactory();
                }

                return instance;
            }
        }

        private EnemyFactory()
        {
            spriteRendererPrototypes = new Dictionary<string, SpriteRenderer>();
            spriteRendererPrototypes.Add("simpleGreen", new SpriteRenderer("enemyGreen1", 1, 0));
            spriteRendererPrototypes.Add("simpleRed", new SpriteRenderer("enemyRed1", 1, 0));
        }

        public override GameObject Create(string type, Vector2 startPos)
        {
            GameObject enemy = new GameObject(startPos);
            enemy.AddComponent(new EasyEnemy());
            
            if (spriteRendererPrototypes.ContainsKey(type))
            {
                enemy.AddComponent(spriteRendererPrototypes[type].Clone());
            }
            else
            {
                enemy.AddComponent(spriteRendererPrototypes["simpleGreen"].Clone());
            }
            ((Enemy)enemy.GetComponent("EasyEnemy")).ChangeState(new MovingDownState());

            return enemy;
        }
    }
}
