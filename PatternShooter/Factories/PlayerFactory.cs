﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternShooter
{
    public class PlayerFactory : Factory
    {
        bool hasCreated = false;
        Dictionary<string, SpriteRenderer> spriteRendererPrototypes;

        static PlayerFactory instance;

        public static PlayerFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PlayerFactory();
                }

                return instance;
            }
        }

        private PlayerFactory()
        {
            spriteRendererPrototypes = new Dictionary<string, SpriteRenderer>();
            spriteRendererPrototypes.Add("normal", new SpriteRenderer("PlayerSheet", 4, 25));
            spriteRendererPrototypes.Add("broken", new SpriteRenderer("PlayerSheetDmg", 4, 25));
            spriteRendererPrototypes.Add("normalStatic", new SpriteRenderer("PlayerSheet", 4, 0));
            spriteRendererPrototypes.Add("brokenStatic", new SpriteRenderer("PlayerSheetDmg", 4, 0));
        }

        public override GameObject Create(string type, Vector2 startPos)
        {
            if (! this.hasCreated)
            {
                GameObject spiller = new GameObject(startPos);
                spiller.AddComponent(new Player());

                if (spriteRendererPrototypes.ContainsKey(type))
                {
                    spiller.AddComponent(spriteRendererPrototypes[type]);
                }
                else
                {
                    spiller.AddComponent(spriteRendererPrototypes["normal"]);
                }

                this.hasCreated = true;

                return spiller;
            }
            else
            {
                throw new NotSupportedException("Only 1 Player can exist!");
            }
            
        }
    }
}
