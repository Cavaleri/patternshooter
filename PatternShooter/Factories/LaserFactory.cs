﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PatternShooter
{
    class LaserFactory : Factory
    {
        Dictionary<string, SpriteRenderer> spriteRendererPrototypes;

        static LaserFactory instance;

        public static LaserFactory Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new LaserFactory();
                }

                return instance;
            }
        }

        private LaserFactory()
        {
            spriteRendererPrototypes = new Dictionary<string, SpriteRenderer>();
            spriteRendererPrototypes.Add("default", new SpriteRenderer("laserRed07", 1, 0));
        }

        public override GameObject Create(Vector2 startPos)
        {
            GameObject lsr = new GameObject(startPos);
            lsr.AddComponent(new Laser(200f, new Vector2(0, -1)));
            lsr.AddComponent(spriteRendererPrototypes["default"]);

            return lsr;
        }
    }
}